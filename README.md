MyPortal is my personal website. It lists my professional background, my
personal interests and some other fun stuff like vocabulary, games etc.

This project is for my personal use and I do not intend to generalize it at
this moment. However, if you are intersted in contributing/improving/suggesting
changes, do reach out to me! My contact information is present on the portal.

The portal is live now! Please visit: http://komandcenter.ddns.net
