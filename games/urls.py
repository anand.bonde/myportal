from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^reset_housie/', views.reset_housie, name='reset_housie'),
	url(r'^housieserver/', views.housie_server, name='housie_server'),
	url(r'^housie/', views.housie, name='housie'),
#	url(r'^housieserver2/', views.housie_server2, name='housie_server2'),
#	url(r'^housie2/', views.housie2, name='housie2'),
    url(r'^$', views.index, name='index'),
]
