from django.shortcuts import render
from random import randint
import os

server_numbers = set()


def reset_housie(request):
    server_numbers.clear();
    next_number = randint(1, 100)
    server_numbers.add(next_number)
    say(next_number)
    return render(request, "games/housie_server.html", {'next_number': next_number, 'server_numbers': sorted(server_numbers)})


def housie_server(request):
    added = 0
    while added == 0:
        next_number = randint(1, 100)
        if next_number not in server_numbers:
            server_numbers.add(next_number)
            added = 1
            say(next_number)
    return render(request, "games/housie_server.html", {'next_number': next_number, 'server_numbers': sorted(server_numbers)})


def housie(request):
    numbers = set()
    for n in range(15):
        added = 0
        while added == 0:
            nxt = randint(1, 100)
            if nxt not in numbers:
                numbers.add(nxt)
                added = 1
    return render(request, "games/housie.html", {'numbers': sorted(numbers)})


def housie_server2(request):
    added = 0
    while added == 0:
        next_number = randint(1, 100)
        if next_number not in server_numbers:
            server_numbers.add(next_number)
            added = 1
            say(next_number)
    return render(request, "games/housie_server2.html", {'next_number': next_number, 'server_numbers': sorted(server_numbers)})


def housie2(request):
    ticket_numbers = set()
    for n in range(15):
        added = 0
        while added == 0:
            next_number = randint(1, 100)
            if next_number not in ticket_numbers:
                ticket_numbers.add(next_number)
                added = 1
    return render(request, "games/housie2.html", {'numbers': sorted(ticket_numbers)})


def index(request):
    return render(request, "games/games.html")


def say(number):
    speech = "~/gitlab/myportal/tts/speech.sh 'number {} ' &".format(number)
    os.system(speech)
