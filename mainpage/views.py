from django.shortcuts import render

def index(request):
    return render(request, "mainpage/index.html")

def contact(request):
    return render(request, "mainpage/basic.html", {"content":['If you would like to contact me, please email me:','anand.bonde@gmail.com']})