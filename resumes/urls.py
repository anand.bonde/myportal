from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^anand/', views.anand, name='anand'),
    url(r'^komal/', views.komal, name='komal'),
]