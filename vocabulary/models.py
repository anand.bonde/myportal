from django.db import models


class Word(models.Model):
    word = models.CharField(max_length=128)
    meaning = models.TextField(max_length=256)
    sentences = models.TextField()
    date = models.DateTimeField()

    def __str__(self):
        return self.word
