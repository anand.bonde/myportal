from django.conf.urls import url, include
from django.views.generic import ListView, DetailView
from . import views
from vocabulary.models import Word

urlpatterns = [
    url(r'^vocabulary/',
        ListView.as_view(queryset=Word.objects.all().order_by("-date")[:25],
                         template_name="vocabulary/vocabulary.html")),
    url(r'^vocabulary/(?P<pk>\d+)$', DetailView.as_view(model=Word,
                                             template_name="vocabulary/word.html")),
    url(r'^vocabularytest/$', views.vocabularytest, name="vocabularytest")
]